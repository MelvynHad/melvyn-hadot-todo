package fr.afpa.cda.todoapp.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import fr.afpa.cda.todoapp.dao.GenericDAOJPAImpl;
import fr.afpa.cda.todoapp.dao.UserDAO;
import fr.afpa.cda.todoapp.model.entities.User;


public class UserDAOImpl extends GenericDAOJPAImpl<User, Integer> implements UserDAO {

	@Override
	public Class<User> getClazz() {
		setClazz(User.class);
		return User.class;
	}
	
	@Override
	public User findUserByLoginAndPassword(String login, String password) {
		User utilisateur = null;
		EntityManager em = getEntityManager();

		try {
			String req = "SELECT u FROM user u WHERE u.login =:login AND u.password =:passwd";
			utilisateur = em.createQuery(req, User.class).setParameter("login", login).setParameter("passwd", password)
					.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			closeEntityManager(em);
		}

		return utilisateur;
	}
	
	@Override
	public List<User> findUserByKeyword(String keyword) {
		String upKeyword = keyword.toUpperCase() ;
		EntityManager em = getEntityManager();
		List<User> utilisateur = new ArrayList<User>();

		try {
			String req = "SELECT u FROM user u WHERE UPPER(u.login) like :keyword";
			utilisateur = em.createQuery(req, User.class).setParameter("keyword", upKeyword+"%").getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			closeEntityManager(em);
		}

		
		return utilisateur;
	}

}
