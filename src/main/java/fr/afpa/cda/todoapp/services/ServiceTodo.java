package fr.afpa.cda.todoapp.services;

import java.util.List;

import fr.afpa.cda.todoapp.model.dto.TodoDTO;
import fr.afpa.cda.todoapp.model.dto.UserDTO;

public interface ServiceTodo {
	
	public List<TodoDTO> getAllTodos();
	public TodoDTO addTodo(TodoDTO todoDto);
	public List<TodoDTO> getTodosByUser (UserDTO userDto);
	public TodoDTO edit(TodoDTO todoDto);
	public void remove(TodoDTO todoDto);
	public void removeTodoById(int id);
	public TodoDTO getTodoById(int id);

}
