package fr.afpa.cda.todoapp.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

public class Mapper {

    public static final ModelMapper mapper = new ModelMapper();

    private Mapper() {
    }

    public static  <T, E> T map(E source, Class<T> destinationType) {
        return destinationType.cast(mapper.map(source, destinationType));
    }
    
    public static <S, T> List<T> mapList(List<S> source, Class<T> destinationType) {
    	return source.stream().map(element -> mapper.map(element, destinationType)).collect(Collectors.toList());
    }  
    	
}
