package fr.afpa.cda.todoapp.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fr.afpa.cda.todoapp.model.dto.TodoDTO;

@Entity(name = "todo")
@Table(name = "todo")
public class Todo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_todo")
	private int idTodo;
	private String description;
	private String title;
	private boolean done;

	@ManyToOne
	@JoinColumn(name = "id_user")
	private User user;

	public Todo() {
	};

	public Todo(int idTodo, String description, String title, boolean done, User user) {
		super();
		this.idTodo = idTodo;
		this.description = description;
		this.title = title;
		this.done = done;
		this.user = user;
	}

	public Todo(String description, String title, boolean done, User user) {
		super();
		this.description = description;
		this.title = title;
		this.done = done;
		this.user = user;
	}
	public int getIdTodo() {
		return idTodo;
	}

	public void setIdTodo(int idTodo) {
		this.idTodo = idTodo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

    @Override
    public boolean equals(Object o) {
	if (o == this)
	    return true;
	if (!(o instanceof Todo))
	    return false;
	Todo other = (Todo) o;
	boolean titleEquals = (this.title == null && other.title == null)
		|| (this.title != null && this.title.equals(other.title));
	boolean descriptionEquals = (this.description == null && other.description == null)
		|| (this.description != null && this.description.equals(other.description));
	boolean UserDTOEquals = (this.user == null && other.user == null)
		|| (this.user != null && this.user.equals(other.user));
	return this.done == other.done && titleEquals && descriptionEquals && UserDTOEquals;
    }

    @Override
    public int hashCode() {
	int result = 17;
	result = 31 * result + Boolean.hashCode(done);
	if (this.title != null)
	    result = 31 * result + this.title.hashCode();
	if (this.description != null)
	    result = 31 * result + this.description.hashCode();
	if (this.user != null)
	    result = 31 * result + this.user.hashCode();
	return result;
    }
}
