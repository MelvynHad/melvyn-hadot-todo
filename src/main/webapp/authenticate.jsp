
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="/header.jsp"></jsp:include>
<div class="container col-md-8 col-md-offset-3" style="overflow: auto">
	<h1>Login</h1>
	<c:if test="${isConnected}"><c:redirect url="app?action=ShowListByUser" /></c:if>
	<form action="app?action=Authenticate" method="post">

		<div class="form-group">
			<label for="idlogin">Login:</label> <input type="text"
				class="form-control" id="idlogin" placeholder="login" name="login"
				required>
		</div>

		<div class="form-group">
			<label for="idpassword">Password:</label> <input type="password"
				class="form-control" id="idpassword" placeholder="Password"
				name="password" required>
		</div>


		<button type="submit" class="btn btn-primary">Submit</button>
	</form>

</div>
</body>

</html>