package fr.afpa.cda.todoapp.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.todoapp.model.dto.TodoDTO;
import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.services.impl.ServiceTodoImpl;

public class CommandShowCreateForm implements Command {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession(true);

		session.setAttribute("path", request.getParameter("path"));

		if ((boolean) session.getAttribute("isConnected")) {

			return "todo-form";

		} else {

			return "authenticate";
		}

	}

}
