package fr.afpa.cda.todoapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import fr.afpa.cda.todoapp.dao.util.DAOUtil;

public abstract class GenericDAOJPAImpl<T, ID> implements DAO<T, ID> {

	private Class<T> clazz = getClazz();
	
	abstract public Class<T> getClazz();

	public final void setClazz(final Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	protected EntityManager getEntityManager() {
		
		EntityManager em = DAOUtil.getEmf().createEntityManager();

		return (em);
	}

	protected void closeEntityManager(EntityManager em) {
		if (em != null) {
			if (em.isOpen()) {
				EntityTransaction t = em.getTransaction();
				if (t.isActive()) {
					try {
						t.rollback();
					} catch (PersistenceException e) {
					}
				}
				em.close();
			}
		}
	}

	public T create(T t) {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(t);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			closeEntityManager(em);
		}
		return t;
	}
	
	
	public void delete(T t) {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.remove(em.merge(t));
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			closeEntityManager(em);
		}
	}

	public void deleteById(ID id) {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			T t = (T) em.find(clazz, id);
			em.remove(t);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e; 
		} finally {
			closeEntityManager(em);
		}
	}
	
	public T update(T t) {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.merge(t);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new RuntimeException();
		} finally {
			closeEntityManager(em);
		}
		return t;
	}

	public T findById(ID id) {
		EntityManager em = getEntityManager();
		T t;
		try {
			t = (T) em.find(clazz, id);
		} finally {
			closeEntityManager(em);
		}
		return t;
	}

	public List<T> findAll() {
		EntityManager em = getEntityManager();
		List<T> list;
		try {
			em.getTransaction().begin();
			String req = "select Object(t) from " + clazz.getName() + " t";
			list = em.createQuery(req, clazz).getResultList();

		} finally {
			closeEntityManager(em);
		}
		return list;
	}

}
