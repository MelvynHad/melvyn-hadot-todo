package fr.afpa.cda.todoapp.controller.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.todoapp.model.dto.TodoDTO;
import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.model.entities.Todo;
import fr.afpa.cda.todoapp.services.ServiceTodo;
import fr.afpa.cda.todoapp.services.impl.ServiceTodoImpl;

/**
 * Show list of todos by user  
 */
public class CommandShowListByUser implements Command {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession uneSession = request.getSession(true);
		
		UserDTO utilisateur = (UserDTO) uneSession.getAttribute("user");
		
		if (utilisateur != null) {
			
			List<TodoDTO> toDos = ServiceTodoImpl.serviceTodo.getTodosByUser(utilisateur);
			uneSession.setAttribute("todos", toDos);

			return "todo-list-by-user";
			
		} else {
			
			return "authenticate";
			
		}

	}
}