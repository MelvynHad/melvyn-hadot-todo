package fr.afpa.cda.todoapp.services;

import java.util.List;

import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.model.entities.User;

public interface ServiceUser {
	
	public UserDTO login(UserDTO user);
	public List<UserDTO> getUserByKeyword(String keyword);
	
}
