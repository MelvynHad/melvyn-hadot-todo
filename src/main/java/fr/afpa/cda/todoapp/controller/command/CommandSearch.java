package fr.afpa.cda.todoapp.controller.command;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.todoapp.model.dto.TodoDTO;
import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.services.ServiceTodo;
import fr.afpa.cda.todoapp.services.ServiceUser;
import fr.afpa.cda.todoapp.services.impl.ServiceTodoImpl;
import fr.afpa.cda.todoapp.services.impl.ServiceUserImpl;

public class CommandSearch implements Command {
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession uneSession = request.getSession(true);
		
		if(request.getParameter("keyword") != null) {
			uneSession.setAttribute("keyword", request.getParameter("keyword"));
		}

		Map<UserDTO, List<TodoDTO>> listesDeToDoParUtils = new HashMap<>();
		
			List<UserDTO> utilisateurs = ServiceUserImpl.serviceUser.getUserByKeyword((String) uneSession.getAttribute("keyword"));
			
			for(UserDTO utilisateur : utilisateurs) {
				listesDeToDoParUtils.put(utilisateur, ServiceTodoImpl.serviceTodo.getTodosByUser(utilisateur));
			}
			
			uneSession.setAttribute("todosByUser", listesDeToDoParUtils);
			
		

		return "todo-list-search";
		
	}
}
