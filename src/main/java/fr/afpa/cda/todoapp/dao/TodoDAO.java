package fr.afpa.cda.todoapp.dao;

import java.util.List;

import fr.afpa.cda.todoapp.model.entities.Todo;
import fr.afpa.cda.todoapp.model.entities.User;


public interface TodoDAO extends DAO<Todo, Integer> {
	
	public List<Todo> findTodosByUser (User user);
	
	

}
