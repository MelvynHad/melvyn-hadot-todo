
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="/header.jsp"></jsp:include>
<div class="container col-md-8 col-md-offset-3" style="overflow: auto">
	<h1>
		Todo
		<c:out value="${todo.title}" />
	</h1>


	<div class="form-group">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th><c:out value="${todo.title}" /></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><c:out value="${todo.description}" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<c:if test="${ path == 'listbyuser' }">
		<form action="app?action=ShowListByUser" method="post">
	</c:if>
	<c:if test="${ path == 'listsearch' }">
		<form action="app?action=Search" method="post">
	</c:if>
	<button type="submit" class="btn btn-primary">Return</button>
	</form>

</div>
</body>

</html>