package fr.afpa.cda.todoapp.services.impl;

import java.util.List;

import org.modelmapper.ModelMapper;

import fr.afpa.cda.todoapp.dao.TodoDAO;
import fr.afpa.cda.todoapp.dao.impl.TodoDAOImpl;
import fr.afpa.cda.todoapp.model.dto.TodoDTO;
import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.model.entities.Todo;
import fr.afpa.cda.todoapp.model.entities.User;
import fr.afpa.cda.todoapp.services.Mapper;
import fr.afpa.cda.todoapp.services.ServiceTodo;

/**
 *Service Todo Implementation
 */
public class ServiceTodoImpl implements ServiceTodo {

	public static final ServiceTodo  serviceTodo = new ServiceTodoImpl();
	
	private TodoDAO todoDAO = new TodoDAOImpl();

	@Override
	public List<TodoDTO> getAllTodos() {
		
		List<Todo> todos = todoDAO.findAll();
		List<TodoDTO> todosDto = Mapper.mapList(todos , TodoDTO.class);
		return todosDto;
		
	}

	@Override
	public TodoDTO addTodo(TodoDTO todoDto) {

		Todo todo = Mapper.map(todoDto, Todo.class);
		Todo persistedTodo = todoDAO.create(todo);
		todoDto = Mapper.mapper.map(persistedTodo, TodoDTO.class);
		return todoDto;
	}

	@Override
	public List<TodoDTO> getTodosByUser(UserDTO userDto) {

		User user = Mapper.mapper.map(userDto, User.class);
		List<Todo> todos = todoDAO.findTodosByUser(user);
		List<TodoDTO> todosDto = Mapper.mapList(todos , TodoDTO.class);
		return todosDto;
	}

	@Override
	public TodoDTO edit(TodoDTO todoDto) {
		
		Todo todo = Mapper.mapper.map(todoDto, Todo.class);
		todo = todoDAO.update(todo);
		todoDto = Mapper.mapper.map(todo, TodoDTO.class);
		return todoDto;
	}

	@Override
	public void remove(TodoDTO todoDto) {
		
		Todo todo = Mapper.mapper.map(todoDto, Todo.class);
		todoDAO.delete(todo);

	}

	@Override
	public void removeTodoById(int id) {
		
		todoDAO.deleteById(id);
	
	}

	@Override
	public TodoDTO getTodoById(int id) {
		
		Todo todo = todoDAO.findById(id);
		TodoDTO todoDto = Mapper.mapper.map(todo, TodoDTO.class);
		
		return todoDto;
	}

}
