<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="/header.jsp"></jsp:include>

<div class="container col-md-5">
	<div class="card">
		<div class="card-body">
			<c:if test="${todo != null}">
				<c:if test="${ path == 'listsearch' }">
					<form action="app?action=UpdateTodo&path=listsearch" method="post">
				</c:if>
				<c:if test="${ path == 'listbyuser' }">
					<form action="app?action=UpdateTodo&path=listbyuser" method="post">
				</c:if>
			</c:if>
			<c:if test="${todo == null}">
				<c:if test="${ path == 'listsearch' }">
					<form action="app?action=CreateTodo&path=listsearch" method="post">
				</c:if>
				<c:if test="${ path == 'listbyuser' }">
					<form action="app?action=CreateTodo&path=listbyuser" method="post">
				</c:if>
			</c:if>

			<caption>
				<h2>
					<c:if test="${todo != null}">
               Edit Todo
              </c:if>
					<c:if test="${todo == null}">
               Add New Todo
              </c:if>
				</h2>
			</caption>

			<fieldset class="form-group">
				<label>Todo Title</label> <input type="text"
					<c:if test="${todo != null}">
      value="<c:out value='${todo.title}' />" readonly </c:if>
					class="form-control" name="title" required="required" minlength="5">
			</fieldset>

			<fieldset class="form-group">
				<label>Todo Description</label> <input type="text"
					value="<c:out value='${todo.description}' />" class="form-control"
					name="description" minlength="5">
			</fieldset>

			<fieldset class="form-group">
				<label>Todo Status</label> <select class="form-control"
					name="isDone">
					<option value="false"
						<c:if test="${!todo.done}">selected=true</c:if>>In
						Progress</option>
					<option value="true" <c:if test="${todo.done}">selected=true</c:if>>Complete</option>
				</select>
			</fieldset>

			<button type="submit" class="btn btn-success">Save</button>
			</form>
		</div>
	</div>
</div>
</body>
</html>