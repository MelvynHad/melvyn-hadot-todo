package fr.afpa.cda.todoapp.constantes;

/**
 * Class to get constant make for URL/URI 
 */
public final class Constantes {

	public static final String CMDPACKAGE = "fr.afpa.cda.todoapp.controller.command.";
	public static final String CMDPARAM = "action";
	public static final String CMD = "Command";

	public static final String JSPROOT = "/";

}
