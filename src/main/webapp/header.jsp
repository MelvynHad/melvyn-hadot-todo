<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>ToDoApp</title>


</head>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark bg-danger">

			<ul class="navbar-nav">
				<li><a href="app?action=ShowListByUser" class="nav-link">Todos</a></li>
			</ul>
			<c:if test = "${user != null}">
			<div class="navbar-nav position-absolute top-50 start-50 translate-middle" >
				<span >Bienvenue <c:out value="${user.firstName} ${user.lastName}"/></span>
			</div>
			</c:if>
			<ul class="navbar-nav navbar-collapse justify-content-end ">
				<form  class="m-2" action="app?action=Search" method="post">
					<input  type="search" name="keyword">
				</form>
				<li><c:if test="${user != null}">
						<a href="app?action=Desauthenticate" class="nav-link">Logout</a>
					</c:if> <c:if test="${user == null}">
						<a href="app?action=Authenticate" class="nav-link">Login</a>
					</c:if></li>
			</ul>

		</nav>
		<style>
tr.barre td {
	background-color: rgba(51, 170, 51, .2);
}

tr.barre td:not(:last-of-type) {
	text-decoration: line-through;
	text-decoration-style: solid;
	text-decoration-color: white;
}
</style>
	</header>