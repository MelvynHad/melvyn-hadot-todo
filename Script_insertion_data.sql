INSERT INTO user_todo (first_name,last_name,password,login) VALUES
('Marie', 'Bracquemond', '123', 'SIAM'),
('Marie2', 'Bracquemond2', '123', 'SIAM2'),
('Quentin2','Moneger2','123','mquentin2'),
('Quentin', 'Moneger', '123', 'mquentin');

INSERT INTO todo (title, done, description, id_user) VALUES 
('titre Quentin 1',false,'description Quentin 1',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='QUENTIN')),
('titre Quentin 2',false,'description Quentin 2',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='QUENTIN')),
('titre Quentin 3',false,'description Quentin 3',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='QUENTIN')),
('titre Quentin 4',false,'description Quentin 4',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='QUENTIN')),
('titre Quentin 5',false,'description Quentin 5',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='QUENTIN')),
('titre Quentin 1',false,'description Quentin 1',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='QUENTIN2')),
('titre Quentin 2',false,'description Quentin 2',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='QUENTIN2')),
('titre Quentin 3',false,'description Quentin 3',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='QUENTIN2')),
('titre Quentin 4',false,'description Quentin 4',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='QUENTIN2')),
('titre Quentin 5',false,'description Quentin 5',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='QUENTIN2')),
('titre Marie 1',false,'description Marie 1',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='MARIE')),
('titre Marie 2',false,'description Marie 2',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='MARIE')),
('titre Marie 3',false,'description Marie 3',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='MARIE')),
('titre Marie 4',false,'description Marie 4',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='MARIE')),
('titre Marie 1',false,'description Marie 1',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='MARIE2')),
('titre Marie 2',false,'description Marie 2',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='MARIE2')),
('titre Marie 3',false,'description Marie 3',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='MARIE2')),
('titre Marie 4',false,'description Marie 4',(SELECT id_user FROM user_todo WHERE UPPER(first_name)='MARIE2'));