package fr.afpa.cda.todoapp.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class UnknownCommand implements Command {

	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		return "error404";
	}

}
