package fr.afpa.cda.todoapp.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import fr.afpa.cda.todoapp.model.dto.UserDTO;

@Entity (name="user")
@Table (name="user_todo")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_user")
	private int idUser;
	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastName;
	private String password;
	private String login;

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLogin() {
 		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}


	public User(){};
	
	public User(String login, String password) {
		this.login=login;
		this.password=password;
		
	}
	
	public User(int idUser, String firstName, String lastName, String password, String login) {
		super();
		this.idUser = idUser;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.login = login;
	}

	public User(String firstName, String lastName, String password, String login) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.login = login;
	}
	
    @Override
    public boolean equals(Object o) {
	if (o == this)
	    return true;
	if (!(o instanceof User))
	    return false;
	User other = (User) o;
	boolean firstNameEquals = (this.firstName == null && other.firstName == null)
		|| (this.firstName != null && this.firstName.equals(other.firstName));
	boolean lastNameEquals = (this.lastName == null && other.lastName == null)
		|| (this.lastName != null && this.lastName.equals(other.lastName));
	boolean passwordEquals = (this.password == null && other.password == null)
		|| (this.password != null && this.password.equals(other.password));
	boolean loginEquals = (this.login == null && other.login == null)
		|| (this.login != null && this.login.equals(other.login));
	return firstNameEquals && lastNameEquals && passwordEquals && loginEquals;
    }

    @Override
    public int hashCode() {
	int result = 17;
	if (this.firstName != null)
	    result = 31 * result + this.firstName.hashCode();
	if (this.lastName != null)
	    result = 31 * result + this.lastName.hashCode();
	if (this.password != null)
	    result = 31 * result + this.password.hashCode();
	if (this.login != null)
	    result = 31 * result + this.login.hashCode();
	return result;
    }
}
