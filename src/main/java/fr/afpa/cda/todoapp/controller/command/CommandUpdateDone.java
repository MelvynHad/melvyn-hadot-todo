package fr.afpa.cda.todoapp.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.todoapp.model.dto.TodoDTO;
import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.services.ServiceTodo;
import fr.afpa.cda.todoapp.services.impl.ServiceTodoImpl;

public class CommandUpdateDone implements Command {
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession uneSession = request.getSession(true);
		UserDTO utilisateur = (UserDTO) uneSession.getAttribute("user");
		int id = Integer.parseInt(request.getParameter("id"));
		boolean estFini = Boolean.valueOf(request.getParameter("done"));

		TodoDTO todoDto = ServiceTodoImpl.serviceTodo.getTodoById(id);
		if (utilisateur.equals(todoDto.getUser())) {

			todoDto.setDone(estFini);
			ServiceTodoImpl.serviceTodo.edit(todoDto);
			
			uneSession.setAttribute( "path" , request.getParameter("path"));
			
			if(request.getParameter("path").equals("listbyuser")) {
				return "redirect:app?action=ShowListByUser";
			} else if(request.getParameter("path").equals("listsearch")) {
				return "redirect:app?action=Search";
			} else {
				
				return "authenticate";
			}

		} else {
			return "authenticate";
		}
	}

}
