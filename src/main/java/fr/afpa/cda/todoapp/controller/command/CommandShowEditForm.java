package fr.afpa.cda.todoapp.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.todoapp.model.dto.TodoDTO;
import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.services.ServiceTodo;
import fr.afpa.cda.todoapp.services.impl.ServiceTodoImpl;

public class CommandShowEditForm implements Command {

	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession uneSession = request.getSession(true);

		int id = Integer.parseInt(request.getParameter("id"));
		TodoDTO toDoExistant = ServiceTodoImpl.serviceTodo.getTodoById(id);
		UserDTO utilisateur = (UserDTO) uneSession.getAttribute("user");

		uneSession.setAttribute("path", request.getParameter("path"));

		if (utilisateur.equals(toDoExistant.getUser())) {
			uneSession.setAttribute("todo", toDoExistant);
			return "todo-form";

		} else {

			return "authenticate";
		}

	}
}
