package fr.afpa.cda.todoapp.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.services.impl.ServiceUserImpl;

public class CommandAuthenticate implements Command {
	
	@Override
    public String execute(HttpServletRequest request, HttpServletResponse response){
    
    	String login = request.getParameter("login");
    	String motDePasse = request.getParameter("password");
    	HttpSession uneSession = request.getSession(true);

    	try {
    		UserDTO utilisateur = new UserDTO(login, motDePasse);
    		UserDTO nouvUtil = ServiceUserImpl.serviceUser.login(utilisateur);
    	
    		uneSession.setAttribute("isConnected", true);
    		uneSession.setAttribute("user", nouvUtil);
    	    
    		uneSession.setAttribute("todosByUser", null);
    		uneSession.setAttribute("todos", null);
    		
    	    return "redirect:app?action=ShowListByUser";

    	} catch (Exception e) {
    		
    		uneSession.setAttribute("isConnected", false);
    		
    		return "authenticate";
    	}
}}
