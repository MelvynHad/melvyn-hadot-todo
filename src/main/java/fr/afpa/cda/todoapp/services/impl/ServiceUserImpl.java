package fr.afpa.cda.todoapp.services.impl;

import java.util.List;

import fr.afpa.cda.todoapp.dao.UserDAO;
import fr.afpa.cda.todoapp.dao.impl.UserDAOImpl;
import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.model.entities.User;
import fr.afpa.cda.todoapp.services.Mapper;
import fr.afpa.cda.todoapp.services.ServiceTodo;
import fr.afpa.cda.todoapp.services.ServiceUser;

/**
 *Service User Implementation
 */
public class ServiceUserImpl implements ServiceUser {

	public static final ServiceUser serviceUser = new ServiceUserImpl();
	
	private UserDAO userDAO = new UserDAOImpl();
	
	@Override
	public UserDTO login(UserDTO userDto) {
		
 		if(userDto.getLogin()==null || userDto.getPassword()==null) {
			throw new RuntimeException();
		}
		else {
		
			User utilisateur = Mapper.mapper.map(userDto, User.class);			
			utilisateur = userDAO.findUserByLoginAndPassword(utilisateur.getLogin(), utilisateur.getPassword());
			UserDTO nouvUtil = Mapper.mapper.map(utilisateur, UserDTO.class);
			return nouvUtil;
		}
	}
	
	public List<UserDTO> getUserByKeyword(String keyword){
	
		List<User> utilisateurs = userDAO.findUserByKeyword(keyword);
		List<UserDTO> usersDto = Mapper.mapList(utilisateurs, UserDTO.class);			
		return usersDto; 
	}
	

}
