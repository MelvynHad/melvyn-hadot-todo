package fr.afpa.cda.todoapp.dao.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DAOUtil {
    private static EntityManagerFactory emf = null;

    private DAOUtil() {
    }
    
    public static synchronized EntityManagerFactory getEmf() {
        if(emf == null){
            emf = Persistence.createEntityManagerFactory("todoApp");
        }
        return emf;
    }
    
    public static synchronized void close(){
        if(emf != null){
            emf.close();
            emf = null;
        }
    }
}
