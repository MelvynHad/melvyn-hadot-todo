package fr.afpa.cda.todoapp.dao;

import java.util.List;

public interface DAO <T,ID>{

		public T create(T object);

		public T update(T object);

		public void delete(T object);

		public void deleteById(ID id);

		public List<T> findAll();

		public T findById(ID id);

		Class<T> getClazz();


	}


