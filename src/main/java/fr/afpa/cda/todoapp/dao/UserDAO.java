package fr.afpa.cda.todoapp.dao;

import java.util.List;

import fr.afpa.cda.todoapp.model.entities.User;

public interface UserDAO extends DAO<User, Integer> {

	public User findUserByLoginAndPassword(String login, String password);
	
	public List<User> findUserByKeyword(String keyword);
}
