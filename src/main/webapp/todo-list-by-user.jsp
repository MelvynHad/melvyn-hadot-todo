
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="/header.jsp"></jsp:include>

<div class="row">
	<!-- <div class="alert alert-success" *ngIf='message'>{{message}}</div> -->

	<div class="container">
		<h3 class="text-center mt-3">List of Todos</h3>
		<div class="container text-left">
			
			<a href="app?action=ShowCreateForm&path=listbyuser" class="btn btn-success">Add Todo</a>
			
			
		</div>
		<br>
		<table class="m-auto w-75 table table-bordered">
			<thead>
				<tr>
					<th>Todo Status</th>
					<th>Title</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>


				<c:forEach var="todo" items="${todos}">
					<tr <c:if test="${todo.isDone()}"> class="barre"</c:if>>
						<td><form
								action="app?action=UpdateDone&id=${todo.getIdTodo()}&done=${!todo.isDone()}&path=listbyuser"
								method="post">
								<input onChange="this.form.submit()" type="checkbox"
									<c:if test="${todo.isDone()}">
								checked</c:if>>
							</form></td>
						<td><a href="app?action=ShowDetails&id=${todo.getIdTodo()}&path=listbyuser"><c:out
									value="${todo.getTitle()}" /></a></td>


						<td><a href="app?action=ShowEditForm&id=${todo.getIdTodo()}&path=listbyuser">Edit</a>
							&nbsp;&nbsp;&nbsp;&nbsp; <a
							href="app?action=DeleteTodo&id=${todo.getIdTodo()}&path=listbyuser">Delete</a></td>

					</tr>
				</c:forEach>


			</tbody>

		</table>
	</div>
</div>
</body>
</html>