<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="/header.jsp"></jsp:include>

<div class="row">
	<!-- <div class="alert alert-success" *ngIf='message'>{{message}}</div> -->

	<div class="container">
		<h3 class="text-center mt-3">List of Todos</h3>
		
		<div class="container text-left"></div>
		<br>
		<c:if test="${todosByUser.size() == 0 }"> 
			<div> 
				<h2 class="text-center">Aucun resultat !</h2>
			</div>
		</c:if>
		<c:forEach var="todoByUser" items="${todosByUser}">
			<table class="m-auto w-75 table table-bordered">
				<thead>
					<tr>
						<th colspan="3"><c:out value="${todoByUser.key.login}" /></th>
					</tr>
					<tr>
						<th>Todo Status</th>
						<th>Title</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>


					<c:forEach var="todo" items="${todoByUser.value}">
						<tr <c:if test="${todo.isDone()}">class ="barre"</c:if>>
							<td><form
									action="app?action=UpdateDone&id=${todo.getIdTodo()}&done=${!todo.isDone()}&path=listsearch"
									method="post">
									<input onChange="this.form.submit()" type="checkbox"
										<c:if test="${todo.isDone()}">checked</c:if>
										<c:if test="${todoByUser.key.login != user.login}">disabled</c:if>>
								</form></td>
							<td><a href="app?action=ShowDetails&id=${todo.getIdTodo()}&path=listsearch"><c:out
										value="${todo.getTitle()}" /></a></td>


							<td><c:if test="${todoByUser.key.login == user.login}">
									<a href="app?action=ShowEditForm&id=${todo.getIdTodo()}&path=listsearch">Edit
									</a>
								&nbsp;&nbsp;&nbsp;&nbsp; <a
										href="app?action=DeleteTodo&id=${todo.getIdTodo()}&path=listsearch">Delete</a>
								</c:if></td>

						</tr>
					</c:forEach>


				</tbody>

			</table>
		</c:forEach>
	</div>
</div>
</body>
</html>