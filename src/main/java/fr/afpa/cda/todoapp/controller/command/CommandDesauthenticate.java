package fr.afpa.cda.todoapp.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CommandDesauthenticate implements Command {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession uneSession = request.getSession(true);
		
		uneSession.setAttribute("user", null);
		uneSession.setAttribute("isConnected", false);
		uneSession.setAttribute("todos", null);
		
		return "authenticate";
	}

}
