package fr.afpa.cda.todoapp.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import fr.afpa.cda.todoapp.dao.GenericDAOJPAImpl;
import fr.afpa.cda.todoapp.dao.TodoDAO;
import fr.afpa.cda.todoapp.model.entities.Todo;
import fr.afpa.cda.todoapp.model.entities.User;


public class TodoDAOImpl extends GenericDAOJPAImpl<Todo, Integer> implements TodoDAO {

	@Override
	public List<Todo> findTodosByUser(User user) {
		
		List<Todo> todos = new ArrayList();
		EntityManager em = getEntityManager();
		
		try {
			String req = "FROM todo t WHERE t.user= :u";
			todos = em.createQuery(req, Todo.class).setParameter("u", user).getResultList();			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		} finally {
			closeEntityManager(em);
		}
		return todos;
	}
	
	@Override
	public Class<Todo> getClazz() {
		setClazz(Todo.class);
		return Todo.class;
	} 



}
