package fr.afpa.cda.todoapp.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.todoapp.model.dto.TodoDTO;
import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.services.ServiceTodo;
import fr.afpa.cda.todoapp.services.impl.ServiceTodoImpl;

public class CommandCreateTodo implements Command {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {

		HttpSession uneSession = request.getSession(true);

		UserDTO utilisateur = (UserDTO) uneSession.getAttribute("user");
		String titre = request.getParameter("title");
		String description = request.getParameter("description");
		boolean fait = Boolean.valueOf(request.getParameter("isDone"));
	
		if(utilisateur == null || titre == "" || description == "" ) {
			return "redirect:app?action=ShowCreateForm&path=listbyuser";
		}

		TodoDTO newTodoDto = new TodoDTO(titre, fait, description, utilisateur);

		if (utilisateur.equals(newTodoDto.getUser())) {

			ServiceTodoImpl.serviceTodo.addTodo(newTodoDto);
			
			if(request.getParameter("path").equals("listbyuser")) {
				return "redirect:app?action=ShowListByUser";
			} else if(request.getParameter("path").equals("listsearch")) {
				return "redirect:app?action=Search";
			} else {
				return "authenticate";
			}

		} else {
			
			return "authenticate";
		}
	}

}
