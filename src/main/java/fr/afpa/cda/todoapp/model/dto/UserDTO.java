package fr.afpa.cda.todoapp.model.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class UserDTO implements Serializable{
    
	private static final long serialVersionUID = -2967121031097895428L;
	private int idUser;
    private String firstName;
    private String lastName;
    private String password;
    private String login;
    private Map<String, TodoDTO> todos = new HashMap<>();
    
    public UserDTO() {}
    
    public UserDTO(String login, String password) {
    	this.login = login;
    	this.password = password;
    }

    public UserDTO(int idUser, String firstName, String lastName, String password, String login,
			Map<String, TodoDTO> todos) {
		super();
		this.idUser = idUser;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.login = login;
		this.todos = todos;
	}

	public UserDTO(String firstName, String lastName, String password, String login, Map<String,TodoDTO> todos) {
	super();
	this.firstName = firstName;
	this.lastName = lastName;
	this.password = password;
	this.login = login;
	this.todos = todos;
    }
	
	public int getIdUser() {
		return idUser;
	}
	
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	
    public String getFirstName() {
	return firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public String getPassword() {
	return password;
    }

    public String getLogin() {
	return login;
    }

    public Map<String,TodoDTO> getTodos() {
	return todos;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public void setLogin(String login) {
	this.login = login;
    }

    @Override
    public boolean equals(Object o) {
	if (o == this)
	    return true;
	if (!(o instanceof UserDTO))
	    return false;
	UserDTO other = (UserDTO) o;
	boolean firstNameEquals = (this.firstName == null && other.firstName == null)
		|| (this.firstName != null && this.firstName.equals(other.firstName));
	boolean lastNameEquals = (this.lastName == null && other.lastName == null)
		|| (this.lastName != null && this.lastName.equals(other.lastName));
	boolean passwordEquals = (this.password == null && other.password == null)
		|| (this.password != null && this.password.equals(other.password));
	boolean loginEquals = (this.login == null && other.login == null)
		|| (this.login != null && this.login.equals(other.login));
	return firstNameEquals && lastNameEquals && passwordEquals && loginEquals;
    }

    @Override
    public int hashCode() {
	int result = 17;
	if (this.firstName != null)
	    result = 31 * result + this.firstName.hashCode();
	if (this.lastName != null)
	    result = 31 * result + this.lastName.hashCode();
	if (this.password != null)
	    result = 31 * result + this.password.hashCode();
	if (this.login != null)
	    result = 31 * result + this.login.hashCode();
	return result;
    }
}
