package fr.afpa.cda.todoapp.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.todoapp.model.dto.TodoDTO;
import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.services.impl.ServiceTodoImpl;

public class CommandUpdateTodo implements Command {
	
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession(true);
		UserDTO user = (UserDTO) session.getAttribute("user");
		TodoDTO todoDto = (TodoDTO) session.getAttribute("todo");
		
		if (user.equals(todoDto.getUser())) {
			int id = todoDto.getIdTodo();
			String titre = request.getParameter("title");
			String description = request.getParameter("description");
			boolean estFini = Boolean.valueOf(request.getParameter("isDone"));

			TodoDTO updatedTodo = new TodoDTO(id, titre, estFini, description, user);

			ServiceTodoImpl.serviceTodo.edit(updatedTodo);

			session.setAttribute("todo", null);

			session.setAttribute( "path" , request.getParameter("path"));
			
			if(request.getParameter("path").equals("listbyuser")) {
				return "redirect:app?action=ShowListByUser";
			} else if(request.getParameter("path").equals("listsearch")) {
				return "redirect:app?action=Search";
			} else {
				return "authenticate";
			}
			
		} else {
			
			return "authenticate";
		}
	}
}
