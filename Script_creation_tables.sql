DROP TABLE IF EXISTS user_todo CASCADE;
DROP TABLE IF EXISTS todo CASCADE;

CREATE TABLE user_todo(
   id_user INT GENERATED ALWAYS AS IDENTITY,
   first_name VARCHAR(50),
   last_name VARCHAR(50),
   password VARCHAR(50),
   login VARCHAR(50),
   PRIMARY KEY(id_user),
   UNIQUE(login)
);

CREATE TABLE todo(
   id_todo INT GENERATED ALWAYS AS IDENTITY,
   title VARCHAR(50),
   done BOOLEAN,
   description VARCHAR(200),
   id_user INTEGER NOT NULL,
   PRIMARY KEY(id_todo),
   FOREIGN KEY(id_user) REFERENCES user_todo(id_user)
);