package fr.afpa.cda.todoapp.model.dto;

import java.io.Serializable;

public class TodoDTO implements Serializable{
	
	private static final long serialVersionUID = 3590034210879565693L;
	private int idTodo;
    private String title;
    private boolean done =false;
    private String description;
    private UserDTO user;

    public TodoDTO() {
    }

    public TodoDTO(int idTodo, String title, boolean status, String description, UserDTO user) {
    this.idTodo = idTodo;
	this.title = title;
	this.done = status;
	this.description = description;
	this.user=user;
    }
    
    public TodoDTO(String title, boolean status, String description, UserDTO user) {
	this.title = title;
	this.done = status;
	this.description = description;
	this.user=user;
    }

    public int getIdTodo() {
		return idTodo;
	}

	public void setIdTodo(int idTodo) {
		this.idTodo = idTodo;
	}

	public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public boolean isDone() {
	return done;
    }

    public void setDone(boolean done) {
	this.done = done;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public UserDTO getUser() {
	return this.user;
    }

    public void setUser(UserDTO UserDTO) {
	this.user = UserDTO;
    }

    @Override
    public boolean equals(Object o) {
	if (o == this)
	    return true;
	if (!(o instanceof TodoDTO))
	    return false;
	TodoDTO other = (TodoDTO) o;
	boolean titleEquals = (this.title == null && other.title == null)
		|| (this.title != null && this.title.equals(other.title));
	boolean descriptionEquals = (this.description == null && other.description == null)
		|| (this.description != null && this.description.equals(other.description));
	boolean UserDTOEquals = (this.user == null && other.user == null)
		|| (this.user != null && this.user.equals(other.user));
	return this.done == other.done && titleEquals && descriptionEquals && UserDTOEquals;
    }

    @Override
    public int hashCode() {
	int resultat = 17;
	resultat = 31 * resultat + Boolean.hashCode(done);
	if (this.title != null)
		resultat = 31 * resultat + this.title.hashCode();
	if (this.description != null)
		resultat = 31 * resultat + this.description.hashCode();
	if (this.user != null)
		resultat = 31 * resultat + this.user.hashCode();
	return resultat;
    }
}
