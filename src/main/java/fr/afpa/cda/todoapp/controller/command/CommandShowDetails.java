package fr.afpa.cda.todoapp.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.cda.todoapp.model.dto.TodoDTO;
import fr.afpa.cda.todoapp.model.dto.UserDTO;
import fr.afpa.cda.todoapp.services.ServiceTodo;
import fr.afpa.cda.todoapp.services.impl.ServiceTodoImpl;


public class CommandShowDetails implements Command {
	
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession(true);
		UserDTO utiliSession = (UserDTO) session.getAttribute("user");
		int id = Integer.parseInt(request.getParameter("id"));
		TodoDTO todoSession = ServiceTodoImpl.serviceTodo.getTodoById(id);
		request.setAttribute("todo", todoSession);
		
		session.setAttribute( "path" , request.getParameter("path"));
		
		return "details";
	}
}
