package fr.afpa.cda.todoapp.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.cda.todoapp.constantes.Constantes;
import fr.afpa.cda.todoapp.controller.command.Command;
import fr.afpa.cda.todoapp.controller.command.UnknownCommand;


@WebServlet(name = "/indexServlet", urlPatterns = "/")
public class FrontController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public FrontController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		Command command = getCommand(request);
		
		String view = command.execute(request, response);
		
		if (view.startsWith("redirect:")) {
			response.sendRedirect(view.split("redirect:")[1]);
		}
		
		else {
			RequestDispatcher dispatcher = request.getRequestDispatcher(Constantes.JSPROOT + view + ".jsp");	
			dispatcher.forward(request, response);
		}
	}

	private Command getCommand(HttpServletRequest request) {
		
		String action = request.getParameter(Constantes.CMDPARAM);
		Class<?> commandClass = getCommandClass(action);
		
		try {
			return (Command) commandClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			System.out.println(e.getMessage());
			throw new RuntimeException(
					"Command class '" + commandClass + "' inconnu . Verifiez le parametre de votre request the request parameter", e);
		}
	}
	private static Class<?> getCommandClass(String commandAction) {
		System.out.println("getcommandclass:" + Constantes.CMDPACKAGE + Constantes.CMD + commandAction);
		
		Class<?> loadedClass;
		try {
			loadedClass = Class.forName(Constantes.CMDPACKAGE + Constantes.CMD + commandAction);
		} catch (ClassNotFoundException e) {
			loadedClass = UnknownCommand.class;
		}
		return loadedClass;
	}

}
